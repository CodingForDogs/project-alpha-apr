from django.urls import path
from .views import LogInFormView, log_out, signup


urlpatterns = [
    path("login/", LogInFormView, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", signup, name="signup"),
]
