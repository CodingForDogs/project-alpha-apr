from django.shortcuts import render, redirect
from .models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from .forms import NewProjectForm

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "projects": projects,
        "tasks": tasks,
    }
    return render(request, "list_projects.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
        "project": project,
    }
    return render(request, "show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = NewProjectForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            description = form.cleaned_data["description"]
            owner = form.cleaned_data["owner"]
            newProject = Project.objects.create(
                name=name,
                description=description,
                owner=owner,
            )
            newProject.owner = request.user
            newProject.save()
            return redirect("/projects/", name="home")
    form = NewProjectForm()
    context = {
        "form": form,
    }
    return render(request, "create_project.html", context)
